// NOTE: the getDeck() and royal() are the in-class exercises,
//  createCardDeck, cardsWorthTen, logCardDeck,
//

// returns point value as int and resolves face cards
function isRoyal(playingCard) {
  var playingCardValue = 0

  if (playingCard === 'A') {
    playingCardValue = 11 ;

    } else if (playingCard === 'K' || playingCard === 'Q'|| playingCard === 'J') {
      playingCardValue = 10 ;
      
    }   else {
          playingCardValue = parseInt(playingCard,10) ;     
    }

  return playingCardValue ;
  }

// IsPoint() Tests
// var cardTest = isRoyal("A") ;
// console.log(cardTest) ;
// console.log(isRoyal("K")) ;
// console.log(isRoyal("9")) ;


function getDeck() {
  let cardNumbers = ['A','2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'] ;
  let suits       = ['Hearts','Diamonds','Spades','Clubs'] ;
  var cards       = [] ;

  cardNumbers.forEach(card => {

    suits.forEach(cardSuit => {
      cards.push({val:isRoyal(card),displayVal:card,suit:cardSuit}) ;
      console.log(cards.length) ;
    }) ;
    
  }) ;
  console.log(cards.length) ;
  return cards ;
}


const blackjackDeck = getDeck();
console.log(blackjackDeck) ;


/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
class CardPlayer {
  myHand = [] ;
  constructor(playerName,myHand) {
    this.name = playerName ;
  }

  drawCard() {
    console.log("Hit me!") ;
    var randomCard = blackjackDeck[Math.floor(Math.random()*blackjackDeck.length)] ;
    this.myHand.push(randomCard) ;
    console.log("random Card:",randomCard) ;
    return randomCard ;
  }

  showHand() {
    console.log("hand:",this.myHand) ;
    return this.myHand ;
    }

  }

// CREATE TWO NEW CardPlayers
const dealer = new CardPlayer("Matt") ;
const player1 = new CardPlayer("Virginia") ;

console.log("Welcome to the table",player1.name) ;

console.log(player1.drawCard()) ;
console.log(dealer.drawCard()) ;
console.log(player1.drawCard()) ;
console.log(dealer.drawCard()) ;



console.log("hmmm... I wonder what the buffet is like at this casino...?",player1.showHand()) ;


/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} blackJackScore
 * @returns {number} blackJackScore.total
 * @returns {boolean} blackJackScore.isSoft
 */
// NOTE: I wrote a face card resolution function isRoyal()
//       that resolves point values.  The calcPoints is 
//       just tallying.

function calcPoints(hand) {
  var cards = hand ;
  var score = 0 ;
  var i;
  var Acesoft = false ;

  //for loop the card array
  for (i = 0; i < cards.length ; i++) {

    if (score > 21 && cards[i].displayVal === "A") { 
      cards[i].val = 1 ;
      Acesoft = true ;
    }

    score += cards[i].val ;    
        }

  return {"total":score,"isSoft":Acesoft} ;

}

console.log("Player1 has",calcPoints(player1.showHand()),"points") ;

/**
 * Determines whether the dealer should draw another card.
 * 
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */

function dealerShouldDraw(dealerHand) {
  var cards = dealerHand ;

  for (i = 0; i < cards.length ; i++) {
    if (cards[i].displayVal === "A" && calcPoints(dealerH) === 17) {
      dealer.drawCard() ;
      console.log("Dealer Drawing") ;
    }
return calcPoints(player1.showHand())
  }
}


console.log("Dealer has:",dealerShouldDraw(dealer.showHand()))


/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} Shows the player's score, the dealer's score, and who wins
 */
const determineWinner = (playerScore, dealerScore) => {
  // CREATE FUNCTION HERE

}

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = (count, dealerCard) => {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = (player) => {
  const displayHand = player.hand.map((card) => card.displayVal);
  console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
}

/**
 * Runs Blackjack Game
 */
const startGame = function() {
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();

  let playerScore = calcPoints(player.hand).total;
  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  console.log(`Player stands at ${playerScore}`);

  let dealerScore = calcPoints(dealer.hand).total;
  while (dealerScore < 21 && dealerShouldDraw(dealer.hand)) {
    dealer.drawCard();
    dealerScore = calcPoints(dealer.hand).total;
    showHand(dealer);
  }
  if (dealerScore > 21) {
    return 'Dealer went over 21 - you win!';
  }
  console.log(`Dealer stands at ${dealerScore}`);

  return determineWinner(playerScore, dealerScore);
}
// console.log(startGame());